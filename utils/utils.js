
/*
 * 验证手机号是否合格
 * true--说明合格
 */
export function isPhone(phoneStr) {
	let myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
	if (!myreg.test(phoneStr)) {
		return false;
	} else {
		return true;
	}
}

/*
 * 验证身份证号是否合格
 * true--说明合格
 */
export function isIdCard(idCardStr) {
	let idcardReg =
		/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
	if (idcardReg.test(idCardStr)) {
		return true;
	} else {
		return false;
	}
}

/**
 * 验证车牌号是否合格
 * true--说明合格
 */
export function isVehicleNumber(vehicleNumber) {
	let xreg = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF]$)|([DF][A-HJ-NP-Z0-9][0-9]{4}$))/;
	let creg = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}$/;
	if (vehicleNumber.length == 7) {
		return creg.test(vehicleNumber);
	} else if (vehicleNumber.length == 8) {
		return xreg.test(vehicleNumber);
	} else {
		return false;
	}
}

/**
 * 验证车牌号是否合格
 */
export function checkVehicleNumber(licensePlate) {
	var result = false;
	if (licensePlate.length >= 7 || licensePlate.length <= 8){
		var express = /^([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{6}|[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1})$/;
		result = express.test(licensePlate);
	}
	return result;
}

/*
 * 验证字符串是否为空（也不能为纯空格）
 * true--说明为空， false--说明不为空
 */
export function isEmptyString(string) {
	if (
		string == undefined ||
		typeof string == 'undefined' ||
		!string ||
		string == null ||
		string == '' ||
		/^\s+$/gi.test(string)
	) {
		return true;
	} else {
		return false;
	}
}

/*
 * 验证是否为数字
 */
export function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

/*
 * 是否为数组
 */
export function isArray(obj) {
	return Object.prototype.toString.call(obj) === '[object Array]';
}

/*
 * 是否空数组
 */
export function isArrayEmpty(val) {
	if (val && val instanceof Array && val.length > 0) {
		return false;
	} else {
		return true;
	}
}


/*
 *保留两位小数
 */

// 截取url后参数 ulr地址 val参数
export function getQueryVariable(url, val) {
	let vars = url.split("?")[1].split("&");
	for (let i = 0; i < vars.length; i++) {
		let pair = vars[i].split("=");
		if (pair[0] == val) {
			return pair[1];
		}
	}
	return (false);
}


/**
 * validate email
 * @param email
 * @returns {boolean}
 */
export function validateEmail (email) {
  const re = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

/**
 * 生成随机len位数字
 */
export function randomLenNum (len, date){
  let random = ''
  random = Math.ceil(Math.random() * 100000000000000).toString().substr(0, len || 4)
  if (date) random = random + Date.now()
  return random
}

export function login (data) { //登录接口 
	const user = encryption({ // 密码加密
		data: data,
		key: '1234567891234567',
		param: ['password']
	})
	user.password = encodeURIComponent(user.password); // http 使用URL传递参数携带特殊字符时需要encodeURIComponent转码
	
	let params = [];
	for (let key in user) { // 参数 query String 
		params.push(`${key}=${user[key]}`)
	}
	
	return params
}

/**
 *加密处理
 */
export const encryption = (params) => {
  let {
    data,
    type,
    param,
    key
  } = params
  const result = JSON.parse(JSON.stringify(data))
  if (type === 'Base64') {
    param.forEach(ele => {
      result[ele] = btoa(result[ele])
    })
  } else {
    param.forEach(ele => {
      var data = result[ele]
      key = CryptoJS.enc.Latin1.parse(key)
      var iv = key
      // 加密
      var encrypted = CryptoJS.AES.encrypt(
        data,
        key, {
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.ZeroPadding
        })
      result[ele] = encrypted.toString()
    })
  }
  return result
}