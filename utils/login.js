// 点击登录
const getLogin = (callback) => {
	
	if (!uni.getStorageSync('token')) {
		uni.showToast({
			title: '您未登录，请先登录！',
			duration: 2000,
			icon: 'none'
		})
		uni.navigateTo({
			// #ifdef MP-WEIXIN || MP-ALIPAY
				url: '/pages/my/login/getPhone',		
			// #endif
			
			//#ifdef APP-PLUS || H5
				url: '/pages/my/login/code',
			// #endif
		})
	} else if (!uni.getStorageSync('userInfo').nickName) {
		// #ifdef MP-WEIXIN
			uni.navigateTo({
				url: '/pages/my/login/getInfo'
			})
		// #endif
		// #ifdef APP-PLUS
			callback()
		// #endif
	} else {
		callback()
	}
}


export default {
	getLogin
}
