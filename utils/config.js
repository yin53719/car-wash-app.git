import dayjs from 'dayjs';

export const domain = "https://furuidacar.vip/api/";

// export const domain = "http://localhost:8080/api/";
export const storeInfo = {
	type: 'gcj02',
	name:'福睿达汽车维修服务部',
	address:' 江苏省盐城市盐都区宝兴路1号',
	latitude: 33.324701,
	longitude: 120.11683
}
/**
 * 预约类型
{{phrase12.DATA}}
预约人
{{thing3.DATA}}
预约电话
{{phone_number5.DATA}}
车牌号
{{car_number9.DATA}}
到店时间
{{time14.DATA}}
 */
export const subscribe = {
	appointmentReminder:{
		id:"2gpq3-oDuBR1v2y9NdMGnCt8_TFlJzjkXjAjGj0cXqg"
	}
}

export function appointmentReminder (obj){
	const templateData = {
		thing3: {
			value: obj.contactName
		},
		car_number9: {
			value: obj.plateNo
		},
		phone_number5: {
			value: obj.mobile
		},
		phrase12:{
			value:obj.orderType == 1?"洗车":"维修保养"
		}
	}
	return templateData;
}
/*
*
车牌号
{{car_number10.DATA}}
服务门店
{{thing9.DATA}}
服务项目
{{thing3.DATA}}
联系电话
{{phone_number2.DATA}}

*/
export function orderSuccess (obj){
	const templateData = {
		car_number10: {
			value: obj.plateNo
		},
		thing9: {
			value: storeInfo.name
		},
		thing3: {
			value: obj.orderType
		},
		phone_number2: {
			value: obj.mobile
		}
	}
	return templateData;
}
/*
客户名称
{{thing1.DATA}}
车牌号码
{{car_number3.DATA}}
客户电话
{{phone_number4.DATA}}
*/
export function orderCancel (obj){
	const templateData = {
		thing1: {
			value: obj.contactName
		},
		car_number3: {
			value: obj.plateNo
		},
		phone_number4: {
			value: obj.mobile
		}
	}
	return templateData;
}


export const sourceList = {
	["h5"]:'h5',
	wexin:'微信小程序',
	alipay:"支付宝小程序"
}


